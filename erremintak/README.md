# Erremintak

## traefik

[https://traefik.io/](https://traefik.io/)


> The Cloud Native Edge Router

> A reverse proxy / load balancer that's easy, dynamic, automatic, fast, full-featured, open source, production proven, provides metrics, and integrates with every major cluster technologies... No wonder it's so popular!


https://docs.traefik.io/#1-launch-trfik-tell-it-to-listen-to-docker
``` yaml
version: '3'

services:
  reverse-proxy:
    image: traefik # The official Traefik docker image
    command: --api --docker --docker.exposedByDefault=false # Enables the web UI and tells Træfik to listen to docker
    ports:
      - "80:80"     # The HTTP port
      - "8080:8080" # The Web UI (enabled by --api)
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock # So that Traefik can listen to the Docker events
```

### WP stack-ean konfigurazio berriak

``` yaml
...
services:
  nginx:
    ...
    networks:
      - traefik
      - default
    labels:
      - "traefik.frontend.rule=Host:nireweb.test"
      - "traefik.enable=true"
      # - "traefik.docker.network=traefik_default"
...

networks:
  traefik:
    external:
      name: traefik_default
```


## jaeger

[https://www.jaegertracing.io/](https://www.jaegertracing.io/)

## Prometheus

[https://prometheus.io/](https://prometheus.io/)

## cilium

[https://cilium.io/](https://cilium.io/)
