module.exports = {
  title: 'UEU Docker eta DevOps ikastaroa',
  description: 'Waddup datboi',
  base: getBase(),
  themeConfig: {
    // repo: 'http://faktoria.talaios.net/tanit/docs',
    // repoLabel: 'Contribute!',
    // editLinks: true,
    // editLinkText: 'Lagundu orrialde hau hobetzen!',
    locales: {
      '/': {
        nav: [
          { text: 'Docker', link: '/docker/' },
          { text: 'docker-compose', link: '/docker-compose/' },
          { text: 'CI / CD', link: '/cicd/' },
          { text: 'Orkestazioa', link: '/orkestazioa/' },
          { text: 'Erremintak', link: '/erremintak/' }
        ],
        sidebar: {
          '/docker/': genSidebarConfig('Docker'),
          '/docker-compose/': genSidebarConfig('docker-compose'),
          '/cicd/': genSidebarConfig('cicd'),
          '/orkestazioa/': genSidebarConfig('Orkestazioa'),
          '/erremintak/': genSidebarConfig('Erremintak')
        }
      }
    }
  }
}

function getBase() {
  if (process.env.DEPLOY_ENV === 'gitlab-pages') {
    return '/ueu-docker-devops/'
  } else {
    return '/'
  }

}

function genSidebarConfig (title) {

  const children = {
    Docker: [
      '',
      'sarrera',
      'edukiontziak-kudeatzen',
      'irudiak-kudeatzen',
      'jolasean-1',
      'datuak-gordetzea',
      'volumeak',
      'irudiak-sortzen',
      'Dockerfile',
      'jolasean-2'
    ],
    'docker-compose': [
      '',
      'docker-compose.yml'
    ],
    Orkestazioa: [
      ''
    ],
    Erremintak: [
      ''
    ],
    cicd: [
      ''
    ]
  }

  return [
    {
      title,
      collapsable: false,
      children: children[title]
    }
  ]
}
